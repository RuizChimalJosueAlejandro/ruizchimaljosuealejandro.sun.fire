<form method="post" action="index.php?c=libros_controller&a=confirma_editar_libro&v=<?php echo $datos['vista']['tipo_vista'];?>&id_libro=<?php echo $datos['libros'][0]['id_libro'];?>"
	class="form-horizontal" role="form">

	<div class="form-group">
		<label for="isbn_libro" class="col-md-4">ISBN libro: <input type="text" value="<?php echo $datos['libros'][0]['isbn_libro']?>" class="form-control col-md-8" name="libro[isbn_libro]" id="isbn_libro" />
		</label>
	</div>

	<div class="form-group">
		<label for="titulo_libro" class="col-md-4">Titulo Libro:  <input type="text" value="<?php echo $datos['libros'][0]['titulo_libro']?>"  class="form-control col-md-8" 	name="libro[titulo_libro]" id="titulo_libro" />
		</label>
	</div>
        <div class="form-group">
		<label for="editorial_libro" class="col-md-4">Editorial Libro:  <input type="text" value="<?php echo $datos['libros'][0]['editorial_libro']?>"  class="form-control col-md-8" 	name="libro[editorial_libro]" id="editorial_libro" />
		</label>
	</div>
         <div class="form-group">
		<label for="anio_publicacion_libro" class="col-md-4">Año publicacion Libro:  <input type="text" value="<?php echo $datos['libros'][0]['anio_publicacion_libro']?>"  class="form-control col-md-8" 	name="libro[anio_publicacion_libro]" id="anio_publicacion_libro" />
		</label>
	</div>
	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Actualizar</button> <a href="index.php?c=libros_controller&a=ver_lista&v=<?php echo $datos['vista']['tipo_vista']; ?>" class="btn btn-warning">Cancelar</a>
        </div>
	</div>

</form>

<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>
