<?php

function iniciar($aplicacion)
{
   
           
    $datos['vista']['titulo'] = 'Login';
    $datos['vista']['cuerpo'] = 'html/inicio/iniciar.php';
    require "aplicacion/vistas/html/base/base.php";
}

function iniciar_sesion($aplicacion){
    
    
    require 'aplicacion/modelos/usuarios_model.php';
    $user=$_POST['usuario'];
    $pass=$_POST['password'];
    $datos=array();
    $resultado=  obtener_usuario($aplicacion, $user);
    
    if($resultado['error']){
       $datos['error']=true;
       $datos['mensajes_error']=$resultado['mensajes_error'];
       $datos['vista']['titulo']="Error-Login-User";
       $datos['vista']['cuerpo']='html/inicio/iniciar.php';       
    }
    else{
    
        $datos['usuarios']=$resultado['datos'];
        if($datos['usuarios'][0]['pass']==$pass){
                $_SESSION['usuario']=$datos['usuarios'][0]['nombre'];
                $_SESSION['user']=$datos['usuarios'][0]['usuario'];
                $_SESSION['pass']=$datos['usuarios'][0]['pass'];
                $_SESSION['rol']=$datos['usuarios'][0]['rol'];
                @$datos['vista']['titulo']="Bienvenido";
               $datos['vista']['cuerpo']='html/inicio/iniciar.php'; 
        }
        else{
            $datos['error']=true;
                $datos['mensajes_error'][0]='El password ingresado no es correcto';
               $datos['vista']['titulo']="Error-Login-Pass";
               $datos['vista']['cuerpo']='html/inicio/iniciar.php';   
            }
    }
    require "aplicacion/vistas/html/base/base.php";
    
}

function cerrar_sesion($aplicacion){
    
     unset($_SESSION["usuario"]); 
     unset($_SESSION["user"]);
     unset($_SESSION["pass"]);
     unset($_SESSION["rol"]);
    session_destroy();
    $datos['vista']['titulo'] = 'Login';
    $datos['vista']['cuerpo'] = 'html/inicio/iniciar.php';
    
    require "aplicacion/vistas/html/base/base.php";
    
}
