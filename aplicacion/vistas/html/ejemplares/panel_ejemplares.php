<?php if($_SESSION['rol']=='admin_level_1'){?>
<div class="row">
	<div class="col-md-2">
		<a href="index.php?c=ejemplares_controller&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="index.php?c=ejemplares_controller&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>
<?php } ?>
<?php foreach ($datos['ejemplares'] as $ejemplar) { ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Autor: <strong><?php echo $ejemplar['observaciones_ejemplar']; ?></strong>
			</div>
			

			<div class="panel-footer clearfix">
				<div class="pull-right">
					<a href="index.php?c=ejemplares_controller&a=ver_ejemplar&v=panel&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-primary btn-xs">Información</a>
                                        <?php if($_SESSION['rol']=='admin_level_1'){ ?>
                                        <a href="index.php?c=ejemplares_controller&a=editar_ejemplar&v=panel&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-default btn-xs">Editar</a>
                                        <a href="index.php?c=ejemplares_controller&a=borrar_ejemplar&v=panel&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-warning btn-xs">Borrar</a>
                                        <?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>
