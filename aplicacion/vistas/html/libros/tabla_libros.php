<?php if($_SESSION['rol']=='admin_level_1'){?>
<div class="row">
	<div class="col-md-2">
		<a href="index.php?c=libros_controller&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="index.php?c=libros_controller&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Libros</caption>
			<thead>
				<tr>
					<th>Titulo Libro</th>
					<th>Datos</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['libros'] as $libro) { ?>
    <tr>
					<td><?php echo $libro['titulo_libro']; ?></td>
                                        <td></td>
					<td>
                                            <a	href="index.php?c=libros_controller&a=ver_libro&v=tabla&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-primary btn-xs">Información</a> 
                                                <?php if($_SESSION['rol']=='admin_level_1'){ ?>
                                            <a	href="index.php?c=libros_controller&a=editar_libro&v=tabla&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-default btn-xs">Editar</a>
                                            <a  href="index.php?c=libros_controller&a=borrar_libro&v=tabla&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-warning btn-xs">Borrar</a>
                                                <?php }?>
                                        </td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>